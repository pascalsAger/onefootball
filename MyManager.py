from  multiprocessing import Manager, Queue, Pool
from multiprocessing.managers import BaseManager, DictProxy
from collections import defaultdict
from transformer import Transformer





class MyManager(BaseManager):
    pass

MyManager.register('defaultdict', defaultdict, DictProxy)