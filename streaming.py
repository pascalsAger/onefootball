import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import socket
import json


# Set up your credentials
with open('/home/monty/config.json') as config_data:
    data = json.load(config_data)

CONSUMER_KEY = data['twitter']['consumer_key']
CONSUMER_SECRET = data['twitter']['consumer_secret']
ACCESS_TOKEN = data['twitter']['access_token']
ACCESS_SECRET = data['twitter']['access_secret']




class TweetsListener(StreamListener):


  def on_data(self, data):
      try:
          msg = json.loads( data )
          #print( msg['text'].encode('utf-8') )
          #self.client_socket.send( msg['text'].encode('utf-8') )
          #s = 'Lazio fans post stickers of Anne Frank #football #fußball #football https://t.co/26apiVWmPA #news #show #calcio #technology #sport #economy #cinema #football #world'
          #self.queue.put(msg['text'].encode('utf-8'))
          #self.queue.put(s.split())
          with open('tweets.txt','a') as tweet:
            tweet.write(msg['text']+"\n")

          return True
      except BaseException as e:
          print("Error on_data: %s" % str(e))
      return True

  def on_error(self, status):
      print(status)
      return True

def streamData():
  auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
  auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
  print('connecting')
  twitter_stream = Stream(auth, TweetsListener(), secure = True)
  twitter_stream.filter(track=[u'#football',u'#fußball',u'#calcio',u'#futebol',u'#futbol'])

if __name__ == "__main__":
  #s = socket.socket()         # Create a socket object
  #host = "127.0.0.1"     # Get local machine name
  #port = 5555                 # Reserve a port for your service.
  #s.bind((host, port))        # Bind to the port

  #print("Listening on port: %s" % str(port))

  #s.listen(5)                 # Now wait for client connection.
  #c, addr = s.accept()        # Establish connection with client.

  #print( "Received request from: " + str( addr ) )


    streamData()

'''    while not stream_queue.Empty():
      print('starting pool')
      pool.apply_async(transform, args=(stream_queue.get(),frequency_dict))'''


'''  if CONSUMER_KEY == data['twitter']['consumer_key']:
    print('They are the same')

  assert(CONSUMER_SECRET == data['twitter']['consumer_secret'])
  assert(ACCESS_TOKEN == data['twitter']['access_token'])
  assert(ACCESS_SECRET == data['twitter']['access_secret'])'''

#'#football','#fußball','#calcio','#futebol','#futbol'