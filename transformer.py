from collections import Counter
from itertools import chain
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from time import sleep
class Transform(object):
    def __init__(self, offset, counter, filters):
        self.offset = offset
        self.counter = counter
        self.filters = filters
    
    def tokenize(self,fname):
        with open(fname) as f:
            f.seek(self.offset,0)
            lines = f.readlines()
            self.offset = f.tell()
            self.counter += Counter([ i for i in chain.from_iterable( map(str.split, lines)) if i in self.filters])
            return self.counter


if __name__ == '__main__':
    transform = Transform(0,Counter(),{'#football','#fußball','#calcio','#futebol','#futbol'})
    while True:
    	wordcloud = WordCloud(width=900,height=500).generate_from_frequencies(transform.tokenize('tweets.txt'))
    	plt.figure(figsize=(15,8))
    	plt.imshow(wordcloud)
    	plt.axis("off")
    	plt.show()
    	sleep(5)